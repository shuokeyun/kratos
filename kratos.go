package kratos

import (
	"flag"
	"math/rand"
	"time"

	"gitee.com/shuokeyun/kratos/server"
)

var conf string

// nolint:gochecknoinits
func init() {
	flag.StringVar(&conf, "conf", "../../configs", "config path, eg: -conf config.yaml")

	locationShanghai, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		panic(err)
	}
	time.Local = locationShanghai
	rand.Seed(time.Now().Unix())
}

func WithConf(c string) {
	conf = c
}

// NewApp 创建app
func NewApp(options ...server.WithOption) *server.App {
	flag.Parse()
	op := []server.WithOption{server.WithConfigPath(conf)}
	op = append(op, options...)
	kra := &server.App{}
	kra.Option(op...)
	kra.LoadConfig()
	if kra.GetLogger() == nil {
		kra.Option(server.WithDefaultLogger())
	}
	return kra
}
