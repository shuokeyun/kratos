package metrics

import (
	prom "github.com/go-kratos/kratos/contrib/metrics/prometheus/v2"
	"github.com/go-kratos/kratos/v2/middleware"
	"github.com/go-kratos/kratos/v2/middleware/metrics"
	"github.com/prometheus/client_golang/prometheus"
)

// Server 服务监控中间件
func Server() middleware.Middleware {
	prometheus.MustRegister(_metricSeconds, _metricRequests)
	return metrics.Server(
		metrics.WithSeconds(prom.NewHistogram(_metricSeconds)),
		metrics.WithRequests(prom.NewCounter(_metricRequests)),
	)
}

var (
	_metricSeconds = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "server",
		Subsystem: "requests",
		Name:      "duration_sec",
		Help:      "server requests duration(sec).",
		Buckets:   []float64{0.005, 0.01, 0.025, 0.05, 0.1, 0.250, 0.5, 1},
	}, []string{"kind", "operation"})

	_metricRequests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "client",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "The total number of processed requests",
	}, []string{"kind", "operation", "code", "reason"})

	/*_metricGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "server",
		Subsystem: "gauge",
		Name:      "system_gauge",
		Help:      "Server status",
	}, []string{"kind", "operation", "code", "reason"})*/
)
