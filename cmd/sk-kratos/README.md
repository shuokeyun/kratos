# Kratos Cmd Tool

kratos 命令行工具，对官方的工具 [kratos](https://github.com/go-kratos/kratos/tree/main/cmd/kratos) 进行了部分修改，新增了创建表结构命令

### mysql表映射结构体生成
```shell
go run main.go table create med_order -c=configs/config.yaml -d=med_ -t=internal/biz
```
参数解析：  
`-c`可选，配置文件，默认configs/config.yaml  
`-d`可选，删除表名前缀，生成的结构体名字将不带这部分字符  
`-p`可选，添加前缀  
`-u`可选，添加后缀  
`-t`可选，输出目录，默认internal/domain  
命令可在项目中任何位置执行，命令将会自动找到项目根目录然后执行命名

快捷方式：
```shell
make table2struct table=tableName
```