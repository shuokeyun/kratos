package table

import (
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/table/create"
	"github.com/spf13/cobra"
)

var CmdTable = &cobra.Command{
	Use:   "table",
	Short: "Create structure mappings for database tables",
	Long:  "Create structure mappings for database tables. Example: sk-kratos table create *",
	Run:   run,
}

func init() {
	CmdTable.AddCommand(create.CmdCreate)
	CmdTable.AddCommand(create.CmdMessage)
}

func run(cmd *cobra.Command, args []string) {

}
