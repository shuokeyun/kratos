package project

import (
	"os"
	"strings"

	"github.com/AlecAivazis/survey/v2"
)

/*
const (
	discoveryNo = iota
	discoveryEtcd
	discoveryConsul
	discoveryNacos
	discoveryZookeeper
)

const (
	serverTypeHttpGrpc = iota
	serverTypeGrpc
	serverTypeHttp
)*/

type ServerOption struct {
	ServerName    string
	NeedMysql     bool
	NeedRedis     bool
	Discovery     int
	ServerType    int
	EnableTracing bool
}

func initServerOption() *ServerOption {
	o := &ServerOption{}
	qs := []*survey.Question{
		{
			Name:      "ServerName",
			Prompt:    &survey.Input{Message: "输入项目名（默认带前缀：gitee.com/shuokeyun）："},
			Validate:  survey.ComposeValidators(survey.Required, survey.MinLength(3)),
			Transform: survey.ToLower,
		}, /*{
			Name:   "NeedMysql",
			Prompt: &survey.Confirm{Message: "是否启用mysql ：", Default: true},
		}, {
			Name:   "NeedRedis",
			Prompt: &survey.Confirm{Message: "是否启用redis ：", Default: true},
		}, {
			Name:   "Discovery",
			Prompt: &survey.Select{Message: "请选择服务发现组件 ：", Options: []string{"无", "etcd", "consul", "nacos", "zookeeper"}, Default: 4},
		}, {
			Name:   "ServerType",
			Prompt: &survey.Select{Message: "服务类型 ：", Options: []string{"http + grpc", "grpc", "http"}},
		}, {
			Name:   "EnableTracing",
			Prompt: &survey.Confirm{Message: "是否启用链路跟踪 ：", Default: true},
		},*/
	}
	survey.Ask(qs, o)
	sl := strings.Split(o.ServerName, "/")
	if len(o.ServerName) < 3 {
		os.Exit(0)
	}
	if !strings.HasSuffix(sl[0], ".com") {
		o.ServerName = "gitee.com/shuokeyun/" + strings.Trim(o.ServerName, "/")
	}
	return o
}
