package check

import (
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

// CmdCheck .
var CmdCheck = &cobra.Command{
	Use:   "check",
	Short: "Check whether the tool is installed",
	Long:  "Check whether the tool is installed. Example: sk-kratos check all",
	Run:   run,
}

func run(cmd *cobra.Command, args []string) {
	tool := "all"
	if len(args) > 0 {
		tool = args[0]
	}
	if tool == "all" {
		toolAll := []string{
			"go",
			"protoc",
			"protoc-gen-go-errors",
			"protoc-gen-go-grpc",
			"protoc-gen-go",
		}
		//go install github.com/go-kratos/kratos/cmd/protoc-gen-go-errors/v2@latest

		//go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.26
		//go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1
		for _, v := range toolAll {
			cw := exec.Command("which", v)
			cw.Stdout = os.Stdout
			cw.Stderr = os.Stderr

			if err := cw.Run(); err != nil {
				if _, ok := err.(*exec.ExitError); ok {
					log.Printf("命令不存在：%s", "proto")
					continue
				}
			}
		}
	}
}
