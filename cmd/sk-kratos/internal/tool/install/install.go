package install

import "github.com/spf13/cobra"

// CmdInstall .
var CmdInstall = &cobra.Command{
	Use:   "install",
	Short: "Install a tool",
	Long:  "Install a tool. Example: sk-kratos tool install",
	Run:   run,
}

func run(cmd *cobra.Command, args []string) {

}
