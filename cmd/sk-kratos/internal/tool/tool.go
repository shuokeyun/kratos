package tool

import (
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/tool/check"
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/tool/install"
	"github.com/spf13/cobra"
)

// CmdTool .
var CmdTool = &cobra.Command{
	Use:   "tool",
	Short: "tool check and install",
	Long:  "tool check and installt. Example: sk-kratos tool check",
	Run:   run,
}

func init() {
	CmdTool.AddCommand(check.CmdCheck)
	CmdTool.AddCommand(install.CmdInstall)
}

func run(cmd *cobra.Command, args []string) {

}
