package base

import (
	"bytes"
	"log"
	"os"
	"os/exec"
	"unicode"
)

func Gofmt(file string) {
	cmd := exec.Command("gofmt", []string{"-w", file}...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		log.Printf("gofmt %s failure : %s \n", file, err.Error())
	}
}

// Camel2Case 驼峰转下划线
func Camel2Case(name string) string {
	buffer := bytes.Buffer{}
	for i, r := range name {
		if unicode.IsUpper(r) {
			if i != 0 {
				buffer.WriteByte('_')
			}
			buffer.WriteRune(unicode.ToLower(r))
		} else {
			buffer.WriteRune(r)
		}
	}
	return buffer.String()
}
