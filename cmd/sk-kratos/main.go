package main

import (
	"log"

	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/project"
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/proto"
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/run"
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/table"
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/tool"
	"gitee.com/shuokeyun/kratos/cmd/sk-kratos/internal/upgrade"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:     "sk-kratos",
	Short:   "sk-Kratos: An elegant toolkit for Go microservices.",
	Long:    `sk-Kratos: An elegant toolkit for Go microservices.`,
	Version: "v1",
}

func init() {
	rootCmd.AddCommand(project.CmdNew)
	rootCmd.AddCommand(proto.CmdProto)
	rootCmd.AddCommand(upgrade.CmdUpgrade)
	rootCmd.AddCommand(run.CmdRun)
	rootCmd.AddCommand(tool.CmdTool)
	rootCmd.AddCommand(table.CmdTable)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
