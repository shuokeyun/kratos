package server

import (
	"gitee.com/shuokeyun/kratos/conf"
	etcd "github.com/go-kratos/kratos/contrib/registry/etcd/v2"
	"github.com/go-kratos/kratos/v2/registry"
	clientv3 "go.etcd.io/etcd/client/v3"
	goodlegrpc "google.golang.org/grpc"
)

// NewRegistrar 服务注册于发现组件
func NewRegistrar(conf *conf.Registry) (registry.Registrar, registry.Discovery, error) {
	if len(conf.GetEtcd().GetAddress()) == 0 {
		return nil, nil, nil
	}
	c, err := clientv3.New(clientv3.Config{
		Endpoints:   conf.GetEtcd().GetAddress(),
		DialOptions: []goodlegrpc.DialOption{goodlegrpc.WithBlock()},
	})
	if err != nil {
		return nil, nil, err
	}
	v := etcd.New(c)
	return v, v, nil
	//if len(conf.GetZookeeper().GetAddress()) == 0 {
	//	return nil, nil, nil
	//}
	//v, err := zookeeper.New(conf.GetZookeeper().GetAddress())
	//if err != nil {
	//	return nil, nil, err
	//}
	//return v, v, nil
}
