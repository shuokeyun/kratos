package server

import (
	"time"

	"github.com/go-kratos/aegis/ratelimit/bbr"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware"
	"github.com/go-kratos/kratos/v2/middleware/metadata"
	"github.com/go-kratos/kratos/v2/middleware/ratelimit"
	"github.com/go-kratos/kratos/v2/middleware/validate"
	"github.com/go-kratos/kratos/v2/transport/grpc"

	"gitee.com/shuokeyun/kratos/conf"
	"gitee.com/shuokeyun/kratos/middleware/logging"
	"gitee.com/shuokeyun/kratos/middleware/metrics"
	"gitee.com/shuokeyun/kratos/middleware/recovery"
	"gitee.com/shuokeyun/kratos/middleware/tracing"
)

// NewGRPCServer new a gRPC server.
func NewGRPCServer(bootConf *conf.Bootstrap, logger log.Logger, m []middleware.Middleware) *grpc.Server {
	c := bootConf.GetServer()
	mid := []middleware.Middleware{
		recovery.Recovery(recovery.WithLogger(logger)),
	}
	if bootConf.GetMiddleware().GetRateLimit() > 0 {
		limit := int(bootConf.GetMiddleware().GetRateLimit())
		if limit > 1 {
			limit -= 1
		}
		mid = append(mid, ratelimit.Server(ratelimit.WithLimiter(bbr.NewLimiter(bbr.WithWindow(time.Millisecond), bbr.WithBucket(limit), bbr.WithCPUThreshold(0)))))
	}
	if bootConf.GetTrace().GetJaeger().GetEndpoint() != "" {
		mid = append(mid, tracing.Server(bootConf.GetTrace()))
	}
	mid = append(mid, logging.Server(logger), metadata.Server(), metrics.Server())
	if bootConf.GetMiddleware().GetValidate() {
		mid = append(mid, validate.Validator())
	}
	mid = append(mid, m...)
	var opts = []grpc.ServerOption{
		grpc.Middleware(mid...),
	}
	if c.Grpc.Network != "" {
		opts = append(opts, grpc.Network(c.Grpc.Network))
	}
	if c.Grpc.Addr != "" {
		opts = append(opts, grpc.Address(c.Grpc.Addr))
	}
	if c.Grpc.Timeout != nil {
		opts = append(opts, grpc.Timeout(c.Grpc.Timeout.AsDuration()))
	}
	srv := grpc.NewServer(opts...)
	return srv
}
